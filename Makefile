clean:
	@ find cms api core -type f -name "*.pyc" -print | xargs rm
	@ find cms api core -type d -name "__pycache__" -print | xargs rm -rf
	@ rm -f "*.pyc"
	@ rm -rf __pycache__

venv:
	@ source venv/bin/activate

webserver:
	@ python run.py

run: clean webserver

validate:
	find cms api core -type f -name "*.py" -print | xargs pep8

.PHONY: clean webserver run validate venv
