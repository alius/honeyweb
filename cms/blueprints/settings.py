import os

from flask import request
from flask import redirect
from flask import render_template
from flask import Blueprint
from flask import url_for

from flask.ext.security import login_required

from core.forms import DeleteModelForm
from core.models import ProductName
from core.models import ProductGroup
from core.models import IngredientGroup
from cms.forms import ProductionSettingsForm
from cms import TEMPLATE_FOLDER


settings_blueprint = Blueprint(
    'settings_blueprint', __name__, template_folder=TEMPLATE_FOLDER)


def _get_form_from_action(request_form):
    action = request_form['action']
    if 'delete' in action:
        return DeleteModelForm(request_form)
    elif 'name' in action:
        return ProductionSettingsForm(request_form)
    elif 'group' in action:
        return ProductionSettingsForm(request_form)
    elif 'ingredient' in action:
        return ProductionSettingsForm(request_form)
    else:
        raise ValueError('Invalid form action')


@settings_blueprint.route('/help', methods=['GET'])
@login_required
def help():
    return render_template('help.html')


@settings_blueprint.route('/production', methods=['GET', 'POST'])
@login_required
def production():
    if request.method == 'GET':
        names = [t for t in ProductName.all()]
        groups = [t for t in ProductGroup.all()]
        ingredients = [t for t in IngredientGroup.all()]
        form = ProductionSettingsForm()
        return render_template('production-settings.html',
                               names=names,
                               ingredients=ingredients,
                               delete_form=DeleteModelForm(),
                               name_form=form,
                               group_form=form,
                               ingredient_form=form,
                               groups=groups)

    form = _get_form_from_action(request.form)
    if not form.validate():
        # TODO: show errors properly
        return "{}".format(form.errors)

    action = request.form['action']
    if action == 'add-name':
        name = ProductName(name=form.name.data)
        name.save()
    elif action == 'add-group':
        group = ProductGroup(name=form.name.data)
        group.save()
    elif action == 'add-ingredient':
        ingredient = IngredientGroup(name=form.name.data)
        ingredient.save()
    elif action == 'edit-group':
        try:
            group = ProductGroup.get(ProductGroup._id == form.id.data)
        except ProductGroup.DoesNotExist:
            # TODO: show errors properly
            return "Invalid group ID"
        group.name = form.name.data
        group.save()
    elif action == 'edit-name':
        try:
            name = ProductName.get(ProductName._id == form.id.data)
        except ProductName.DoesNotExist:
            # TODO: show errors properly
            return "Invalid type ID"
        name.name = form.name.data
        name.save()
    elif action == 'edit-ingredient':
        try:
            ingredient = IngredientGroup.get(
                IngredientGroup._id == form.id.data)
        except IngredientGroup.DoesNotExist:
            # TODO: show errors properly
            return "Invalid type ID"
        name.name = form.name.data
        name.save()

    return redirect(url_for('settings_blueprint.production'))
