import os

from itertools import chain

from flask import request
from flask import redirect
from flask import render_template
from flask import Blueprint

from flask.ext.security import Security
from flask.ext.security import login_required
from flask.ext.security import utils

from core.forms import (
    SupplierForm, HoneyForm, BerryForm, DeleteModelForm, ProductBatchForm,
    CustomerForm)
from core.database import db
from core import models
from cms import TEMPLATE_FOLDER


cms_blueprint = Blueprint(
    'cms_blueprint', __name__, template_folder=TEMPLATE_FOLDER)


def _get_model_from_action(action):
    if 'honey' in action:
        return models.HoneyBatch
    elif 'berries' in action:
        return models.BerryBatch
    elif 'supplier' in action:
        return models.Supplier
    else:
        raise ValueError('Invalid form action')


def _get_form_from_action(request_form):
    action = request_form['action']
    if 'delete' in action:
        return DeleteModelForm(request_form)
    elif 'honey' in action:
        return HoneyForm(request_form)
    elif 'berries' in action:
        return BerryForm(request_form)
    elif 'supplier' in action:
        return SupplierForm(request_form)
    else:
        raise ValueError('Invalid form action')


def _get_all_products():
    return [p for p in models.Product.group_and_avail(
            models.Product.name)]


@cms_blueprint.route('/', methods=['GET'])
@login_required
def index():
    ''' Show main template
    '''
    return render_template('index.html')


@cms_blueprint.route('/logout', methods=['GET'])
@login_required
def logout():
    utils.logout_user()
    return redirect('/')


@cms_blueprint.route('/suppliers', methods=['GET', 'POST'])
@login_required
def suppliers():
    if request.method == 'GET':
        form = SupplierForm()
        delete_form = DeleteModelForm()
        suppliers = [s for s in models.Supplier.all()]
        return render_template(
            'suppliers.html', suppliers=suppliers, form=form,
            delete_form=delete_form)

    # create, update, or delete supplier
    form = _get_form_from_action(request.form)
    if not form.validate():
        # TODO: show errors properly
        return "{}".format(form.errors)

    action = request.form['action']
    model = _get_model_from_action(action)

    if 'add' in action:
        # creating new one
        supplier = model(name=form.name.data,
                         company_name=form.company_name.data,
                         address=form.address.data,
                         country=form.country.data,
                         email=form.email.data,
                         phone=form.phone.data,
                         comment=form.comment.data,
                         story=form.story.data)
    else:
        try:
            supplier = model.get(model._id == form.id.data)
        except model.DoesNotExist:
            raise Exception('Not found')

        if 'delete' in action:
            supplier.delete_instance()
            return redirect('/suppliers')
        else:
            # updating existing supplier
            supplier.name = form.name.data
            supplier.company_name = form.company_name.data
            supplier.email = form.email.data
            supplier.phone = form.phone.data
            supplier.address = form.address.data
            supplier.country = form.country.data
            supplier.comment = form.comment.data
            supplier.story = form.story.data

    supplier.save()
    return redirect('/suppliers')


@cms_blueprint.route('/ingredients', methods=['GET', 'POST'])
@login_required
def ingredients():
    if request.method == 'GET':
        suppliers = [s for s in models.Supplier.all()]
        ingredient_groups = [g for g in models.IngredientGroup.all()]
        honey_batches = [h for h in models.HoneyBatch.all()]
        berry_batches = [b for b in models.BerryBatch.all()]
        honey_form = HoneyForm()
        berry_form = BerryForm()
        delete_form = DeleteModelForm()
        return render_template(
            'ingredients.html', suppliers=suppliers, honey_form=honey_form,
            berry_form=berry_form, honey_batches=honey_batches,
            berry_batches=berry_batches, delete_form=delete_form,
            ingredient_groups=ingredient_groups)

    form = _get_form_from_action(request.form)
    if not form.validate():
        # TODO: show errors properly
        return "{}".format(form.errors)

    action = request.form['action']
    model = _get_model_from_action(action)
    if form.id.data:
        try:
            batch = model.get(model._id == form.id.data)
        except model.DoesNotExist:
            # TODO: show errors properly
            return 'Invalid ID'
    else:
        batch = model()

    if 'delete' in action:
        batch.delete_instance()
        return redirect('/ingredients')

    try:
        group = models.IngredientGroup.get(
            models.IngredientGroup._id == form.group.data)
    except models.IngredientGroup.DoesNotExist:
        # TODO: show errors properly
        return 'Invalid ingredient group'

    try:
        supplier = models.Supplier.get(
            models.Supplier._id == form.supplier.data)
    except models.Supplier.DoesNotExist:
        # TODO: show errors properly
        return 'Invalid supplier'

    batch.name = form.name.data
    batch.price = form.price.data
    batch.weight = form.weight.data
    batch.purchased = form.purchased.data
    batch.expires = form.expires.data
    batch.group = group
    batch.supplier = supplier

    if form.location.data:
        batch.location = form.location.data
    if form.location_zoom.data:
        batch.location_zoom = form.location_zoom.data
    if hasattr(form, 'is_frozen') and form.is_frozen.data:
        batch.is_frozen = form.is_frozen.data
    if hasattr(form, 'dry_weight') and form.dry_weight.data:
        batch.dry_weight = form.dry_weight.data

    batch.save()
    return redirect('/ingredients')


@cms_blueprint.route('/products', methods=['GET', 'POST'])
@login_required
def products():
    batch_form = ProductBatchForm(request.form)
    components = [c for c in chain(
        models.HoneyBatch.all(), models.BerryBatch.all()) if c._id]
    component_choices = [('{}.{}'.format(c.__class__.__name__, c._id),
                         '{}, {}'.format(c.name, c.purchased)) for c in
                         components]

    if request.args and request.method == 'POST':
        # A batch sale happened
        args = set(request.args)
        if not args.issubset(models.Product.attributes()):
            # TODO: proper error handling
            return 'Invalid search terms'

        product_choices = [(str(p._id), str(p._id)) for p in
                           models.Product.find(**request.args.to_dict())]
        customer_form = CustomerForm(request.form)
        customer_form.serial.choices = product_choices
        if not customer_form.validate():
            # TODO: show errors properly
            return 'error: {}'.format(customer_form.errors)

        # check if it is an existing client
        if customer_form.client_id.data:
            # TODO: implement using client id
            return 'not implemented'
        else:
            # Check if there is a client with the same name and address
            # XXX: it looks for exact match, would be good to later implement
            # a fuzzy search algorithm
            customers = models.Customer.find(
                name=customer_form.name.data,
                address=customer_form.address.data,
                email=customer_form.email.data)
            customer_count = len(customers)
            customer = None
            if customer_count > 1:
                # TODO: handle multiple customers case
                return 'multiple customers error'
            elif customer_count == 1:
                customer = customers[0]
            elif customer_count == 0:
                # create a new customer
                customer = models.Customer(
                    name=customer_form.name.data,
                    type=customer_form.type.data,
                    address=customer_form.address.data,
                    country=customer_form.country.data,
                    email=customer_form.email.data,
                    phone=customer_form.phone.data,
                    comment=customer_form.comment.data)
                customer.save()

        products = [models.Product.get(models.Product._id == pid) for pid in
                    customer_form.serial.data]
        for p in products:
            p.available = False
            p.save()

            s = models.Sale(product=p,
                            customer=customer,
                            price=customer_form.price.data,
                            discount=customer_form.discount.data)
            s.save()

        return redirect(request.url)
    elif request.method == 'POST':
        batch_form.components.choices = component_choices

        if not batch_form.validate():
            # TODO: show errors properly
            return 'error: {}'.format(batch_form.errors)

        ingredients = []
        for chosen_component in batch_form.components.data:
            cls_name, id = chosen_component.split('.')
            try:
                cls = getattr(models, cls_name)
            except AttributeError:
                # TODO: show errors properly
                return 'Invalid component'

            try:
                component = cls.get(cls._id == id)
            except cls.DoesNotExist:
                # TODO: show errors properly
                return 'Invalid component'

            ingredients.append(component)

        for _ in range(batch_form.count.data):
            # create all the jars
            p = models.Product(
                name=batch_form.name.data,
                created=batch_form.created.data,
                expires=batch_form.expires.data,
                group=batch_form.group.data,
                weight=batch_form.weight.data,
                components=ingredients,
                available=True,
                serial=models.Product.generate_serial())
            p.save()
    elif request.args:
        customer_form = CustomerForm()
        products = [p for p in
                    models.Product.find(**request.args.to_dict())]
        products.sort(key=lambda p: p.available, reverse=True)
        return render_template('products.html', products=products,
                               customer_form=customer_form)

    names = [t for t in models.ProductName.all()]
    groups = [t for t in models.ProductGroup.all()]
    products = _get_all_products()

    return render_template('product-batches.html', form=batch_form,
                           components=components, products=products,
                           groups=groups, names=names)


@cms_blueprint.route('/customers', methods=['GET'])
@login_required
def customers():
    return render_template('customers.html')


@cms_blueprint.route('/sales', methods=['GET'])
@login_required
def sales():
    return render_template('sales.html')
