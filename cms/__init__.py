import os


TEMPLATE_FOLDER = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'templates')
