from flask_wtf import Form
from wtforms import IntegerField, StringField, validators


class ProductionSettingsForm(Form):
    id = IntegerField('Id',
                      [validators.Optional(),
                       validators.NumberRange(min=1)])
    name = StringField('Name',
                       [validators.Length(max=64),
                        validators.Optional()])
