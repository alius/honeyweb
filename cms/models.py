from core.database import db
from core.models import ModelMixin

from peewee import (
    Model, CharField, TextField, BooleanField, DateTimeField, ForeignKeyField)
from flask.ext.security import (
    UserMixin, RoleMixin, PeeweeUserDatastore)


class Role(ModelMixin, RoleMixin):
    name = CharField(unique=True)
    description = TextField(null=True)


class User(ModelMixin, UserMixin):
    email = TextField()
    password = TextField()
    active = BooleanField(default=True)
    confirmed_at = DateTimeField(null=True)


class UserRoles(ModelMixin):
    # Because peewee does not come with built-in many-to-many
    # relationships, we need this intermediary class to link
    # user to roles.
    user = ForeignKeyField(User, related_name='roles')
    role = ForeignKeyField(Role, related_name='users')
    name = property(lambda self: self.role.name)
    description = property(lambda self: self.role.description)
