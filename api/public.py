from flask import request
from flask import Blueprint

from api import json_response
from api import NotFoundError
from core import models


public_blueprint = Blueprint("public_api", __name__)


@public_blueprint.route('/jar/<serial>', methods=['GET'])
@json_response
def get_jar(serial):
    try:
        jar = models.Jar.get(models.Jar.serial == serial)
    except models.Jar.DoesNotExist:
        raise NotFoundError('Not found')

    date_fmt = '{:%Y-%m-%d}'

    data = {'serial': serial,
            'created': date_fmt.format(jar.created),
            'expires': date_fmt.format(jar.expires),
            'type': str(jar.type),
            'weight': jar.weight,
            'name': str(jar.name),
            'components': []}

    for component in jar.components:
        supplier = component.supplier
        s = {'name': supplier.name or supplier.company_name,
             'country': supplier.country,
             'story': supplier.story}
        c = {'name': component.name,
             'group': component.group.name,
             'purchased': date_fmt.format(component.purchased),
             'location': component.location,
             'location_zoom': component.location_zoom,
             'supplier': s}
        data['components'].append(c)

    return data
