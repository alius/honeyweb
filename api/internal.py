from itertools import chain

from flask import request
from flask import Blueprint

from core import forms
from api import json_response
from api import NotFoundError
from api import ApiError
from core import models


internal_blueprint = Blueprint('internal_api', __name__)


@internal_blueprint.route('/customers', methods=['GET'])
@json_response
def get_customers():
    if request.args:
        # check for
        args = set(request.args)
        if not args.issubset(models.Customer.attributes()):
            # TODO: proper error handling
            return 'Invalid search terms'
        # search for specific customer
        return [c.to_dict() for c in models.Customer.search(**request.args)]
    else:
        return [c.to_dict() for c in models.Customer.all()]


@internal_blueprint.route('/suppliers', methods=['GET', 'POST'])
@json_response
def create_or_get_suppliers():
    ''' Creates a new component supplier or fetches a list of existing ones.
    '''
    if request.method == 'POST':
        form = forms.SupplierForm(request.form)
        if not form.validate():
            raise ApiError(
                'Error fields: {}'.format(', '.join(form.errors)))
        supplier = models.Supplier(name=form.name.data,
                                   address=form.address.data,
                                   email=form.email.data,
                                   phone=form.phone.data)
        supplier.save()
        return supplier.to_dict()
    else:
        return [supplier.to_dict() for supplier in models.Supplier.all()]


@internal_blueprint.route('/suppliers/<int:id>',
                          methods=['GET', 'PUT', 'DELETE'])
@json_response
def get_or_update_supplier(id):
    ''' Fetches or updates information about the specific supplier
    '''
    if request.method == 'GET':
        try:
            supplier = models.Supplier.get(models.Supplier._id == id)
        except models.Supplier.DoesNotExist:
            raise NotFoundError('Not found')
        return supplier.to_dict()
    elif request.method == 'PUT':
        # TODO: update the batch
        pass
    else:
        # delete supplier
        try:
            supplier = models.Supplier.get(models.Supplier._id == id)
        except models.Supplier.DoesNotExist:
            raise NotFoundError('Not found')
        supplier.delete_instance()


@internal_blueprint.route('/components/honey', methods=['GET', 'POST'])
@json_response
def create_or_get_honey():
    ''' Creates a new batch of honey or fetches a list of all existing ones.
    TODO: there is no pagination at the moment
    '''
    if request.method == 'POST':
        form = forms.HoneyForm(request.form)
        if not form.validate():
            raise ApiError(
                'Error fields: {}'.format(', '.join(form.errors)))
        try:
            _ = models.Supplier.get(models.Supplier._id == form.supplier.data)
        except models.Supplier.DoesNotExist:
            raise ApiError(
                'Error fields: supplier. Invalid supplier')

        batch = models.HoneyBatch(name=form.name.data,
                                  price=form.price.data,
                                  weight=form.weight.data,
                                  purchased=form.purchased.data,
                                  expires=form.expires.data,
                                  supplier_id=form.supplier.data,
                                  location=form.location.data)
        batch.save()
        return batch.to_dict()
    else:
        return [batch.to_dict() for batch in models.HoneyBatch.all()]


@internal_blueprint.route('/components/honey/<int:id>',
                          methods=['GET', 'PUT', 'DELETE'])
@json_response
def get_or_update_honey(id):
    ''' Fetches or updates information about the specific batch of honey
    '''
    if request.method == 'GET':
        try:
            batch = models.HoneyBatch.get(models.HoneyBatch._id == id)
        except models.HoneyBatch.DoesNotExist:
            raise NotFoundError('Not found')
        return batch.to_dict()
    elif request.method == 'POST':
        # TODO: update the batch
        pass
    else:
        # delete honey batch
        try:
            batch = models.HoneyBatch.get(models.HoneyBatch._id == id)
        except models.HoneyBatch.DoesNotExist:
            raise NotFoundError('Not found')
        batch.delete_instance()


@internal_blueprint.route('/components/berries', methods=['GET', 'POST'])
@json_response
def create_or_get_berries():
    ''' Creates a new batch of berries or fetches a list of all existing ones.
    TODO: there is no pagination at the moment
    '''
    if request.method == 'POST':
        form = forms.BerryForm(request.form)
        if not form.validate():
            raise ApiError(
                'Error fields: {}'.format(', '.join(form.errors)))

        try:
            _ = models.Supplier.get(models.Supplier._id == form.supplier.data)
        except models.Supplier.DoesNotExist:
            raise ApiError(
                'Error fields: supplier. Invalid supplier')

        batch = models.BerryBatch(name=form.name.data,
                                  price=form.price.data,
                                  weight=form.weight.data,
                                  purchased=form.purchased.data,
                                  expires=form.expires.data,
                                  is_frozen=form.is_frozen.data,
                                  supplier_id=form.supplier.data,
                                  location=form.location.data)
        batch.save()
        return batch.to_dict()
    else:
        return [batch.to_dict() for batch in models.BerryBatch.all()]


@internal_blueprint.route('/components/berries/<int:id>',
                          methods=['GET', 'PUT'])
@json_response
def get_or_update_berries(id):
    ''' Fetches or updates information about the specific batch of berries
    '''
    if request.method == 'GET':
        try:
            batch = models.BerryBatch.get(models.BerryBatch._id == id)
        except models.BerryBatch.DoesNotExist:
            raise NotFoundError('Not found')
        return batch.to_dict()
    else:
        # TODO: update the batch
        pass


@internal_blueprint.route('/jars', methods=['GET', 'POST'])
@json_response
def create_or_get_jars():
    ''' Creates a new jar of fetches the existing ones.
    TODO: there is no pagination at the moment
    '''
    if request.method == 'POST':
        form = forms.JarForm(request.form)
        # build the list of components for choices
        # this might become slow when the number of records is too big
        choices = [('{}.{}'.format(c.__class__.__name__, c._id),
                   '{}.{}'.format(c.__class__.__name__, c._id)) for c in
                   chain(models.HoneyBatch.all(), models.BerryBatch.all())
                   if c._id]
        if not choices:
            raise ApiError('No components in the database')

        form.components.choices = choices

        if form.validate():
            # load component models
            components = []
            for component_field in form.components.data:
                components.append(component_field.split('.'))

            jar = models.Jar(serial=form.serial.data,
                             created=form.created.data,
                             expires=form.expires.data,
                             type=form.type.data,
                             weight=form.weight.data,
                             components=components)
            jar.save()
            return jar.to_dict()
        else:
            raise ApiError(
                'Error fields: {}'.format(', '.join(form.errors)))
    else:
        return [jar.to_dict() for jar in models.Jar.all()]
