import json
from functools import wraps

from flask import make_response
from flask import request


class ApiError(Exception):
    response_code = 500


class NotFoundError(ApiError):
    response_code = 404


def json_response(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        # XXX: this is a naive implementation of status codes
        response_code = {
            'GET': 200, 'POST': 201, 'PUT': 200, 'DELETE': 204}.get(
                request.method, 200)

        try:
            response = fn(*args, **kwargs)
        except ApiError as e:
            # TODO: use the response code
            response = {'error': str(e)}
            response_code = e.response_code
        except Exception as e:
            # TODO: remove the print statement
            print(e)
            response = {'error': 'Internal Server Error'}
            response_code = 500

        r = make_response(json.dumps(response))
        r.status_code = response_code
        r.mimetype = 'application/json'
        return r
    return wrapper
