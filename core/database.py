import os

from peewee import SqliteDatabase


db_path = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), '../hive.db')
db = SqliteDatabase(db_path, threadlocals=True)
