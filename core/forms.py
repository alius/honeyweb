from flask_wtf import Form
from wtforms import (
    validators, StringField, DateField, IntegerField, FloatField,
    BooleanField, SelectMultipleField, TextField, SelectField)


class CustomerForm(Form):
    client_id = IntegerField('Customer Id', [validators.Optional()])
    serial = SelectMultipleField('Serial',
                                 [validators.DataRequired()])
    name = StringField('Name',
                       [validators.Length(max=256),
                        validators.DataRequired()])
    type = SelectField('Type', [validators.DataRequired()],
                       choices=[('private', 'Private'),
                                ('corporate', 'Corporate')])
    address = StringField('Address',
                          [validators.Length(max=256),
                           validators.Optional()])
    country = StringField('Country',
                          [validators.Length(max=256),
                           validators.DataRequired()])
    email = StringField('Email',
                        [validators.Length(max=256),
                         validators.Optional()])
    phone = StringField('Phone',
                        [validators.Length(max=64),
                         validators.Optional()])
    price = FloatField('Price', [validators.DataRequired()])
    comment = TextField('Comment', [validators.Optional()])
    discount = FloatField('Discount', [validators.Optional()])


class SupplierForm(Form):
    id = IntegerField('Id',
                      [validators.Optional(),
                       validators.NumberRange(min=1)])
    name = StringField('Name',
                       [validators.Length(max=256),
                        validators.Optional()])
    company_name = StringField('Company Name',
                               [validators.Length(max=256),
                                validators.Optional()])
    address = StringField('Address',
                          [validators.Length(max=1024),
                           validators.Optional()])
    country = StringField('Country',
                          [validators.Length(max=1024),
                           validators.Optional()])
    email = StringField('Email',
                        [validators.Length(max=64),
                         validators.Optional(),
                         validators.Email()])
    phone = StringField('Phone',
                        [validators.Length(max=32),
                         validators.Optional()])
    comment = StringField('Comment', [validators.Length(max=1024)])
    story = TextField('Story', [validators.Optional()])


class ProductBatchForm(Form):
    name = StringField('Name',
                       [validators.Length(max=128),
                        validators.DataRequired()])
    created = DateField('Created',
                        [validators.DataRequired()], format='%Y-%m-%d')
    expires = DateField('Expires',
                        [validators.DataRequired()], format='%Y-%m-%d')
    group = StringField('Group', [validators.DataRequired()])
    weight = FloatField('Weight (single itm)', [validators.DataRequired()])
    count = IntegerField('Count', [validators.DataRequired()])
    components = SelectMultipleField('Components')


class ProductForm(Form):
    name = IntegerField('Name',
                        [validators.Length(max=128),
                         validators.DataRequired()])
    serial = StringField('Serial',
                         [validators.Length(max=10),
                          validators.DataRequired()])
    created = DateField('Created',
                        [validators.DataRequired()], format='%Y-%m-%d')
    expires = DateField('Expires',
                        [validators.DataRequired()], format='%Y-%m-%d')
    group = IntegerField('Group', [validators.DataRequired()])
    weight = FloatField('Weight', [validators.DataRequired()])
    components = SelectMultipleField('Components')


class DeleteModelForm(Form):
    id = IntegerField('Id',
                      [validators.Optional(),
                       validators.NumberRange(min=1)])


class IngredientForm(Form):
    id = IntegerField('Id',
                      [validators.Optional(),
                       validators.NumberRange(min=1)])
    name = StringField('Name',
                       [validators.Length(max=256), validators.DataRequired()])
    group = IntegerField('Group', [validators.DataRequired()])
    price = FloatField('Price')
    weight = FloatField('Weigth', [validators.DataRequired()])
    supplier = IntegerField('Supplier', [validators.DataRequired()])
    purchased = DateField('Purchased',
                          [validators.DataRequired()], format='%Y-%m-%d')
    expires = DateField('Expires',
                        [validators.DataRequired()], format='%Y-%m-%d')
    location = StringField('Location (lat, lon)',
                           [validators.Length(max=21), validators.Optional()])
    location_zoom = IntegerField('Location Zoom',
                                 [validators.Optional(),
                                  validators.NumberRange(min=5, max=18)])


class HoneyForm(IngredientForm):
    pass


class BerryForm(IngredientForm):
    dry_weight = FloatField('Dry Weight', [validators.Optional()])
    is_frozen = BooleanField('Frozen', false_values=('0',))
