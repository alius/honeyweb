import os
import json
import string
import random

from datetime import datetime

from peewee import (
    DateField, PrimaryKeyField, CharField, IntegerField, ForeignKeyField,
    FloatField, TextField, BooleanField, fn, Model)
from playhouse.shortcuts import cast

from core.database import db


def _create_tables():
    ''' Create all the necessary tables '''
    db.connect()
    db.create_tables([HoneyBatch, BerryBatch, Product, Supplier, ProductName,
                      ProductGroup, IngredientGroup, Customer, Sale])


def _recreate_tables():
    ''' Drops tables and creates them again '''
    db.connect()
    db.drop_tables([HoneyBatch, BerryBatch, Product, Supplier, ProductName,
                    ProductGroup, IngredientGroup, Customer, Sale])
    db.create_tables([HoneyBatch, BerryBatch, Product, Supplier, ProductName,
                      ProductGroup, IngredientGroup, Customer, Sale])


class ModelMixin(Model):
    class Meta:
        database = db


class AdvancedModel(ModelMixin):
    ''' Class to add helper methods to all models '''
    _id = PrimaryKeyField()

    def to_dict(self):
        return {'_id': self._id}

    @classmethod
    def attributes(cls):
        ''' A helper method used for security check when doing a search '''
        attr = []
        for k, v in cls.__dict__.items():
            if k[0].isalpha() and k[0].islower() \
                    and not hasattr(v, '__call__'):
                attr.append(k)
        return set(attr)

    @classmethod
    def all(cls):
        return cls.select()

    @classmethod
    def search(cls, **kw):
        ''' Search is using string matching '''
        q = cls.select()
        for k, v in kw.items():
            match = '%{}%'.format(v)
            q.where(getattr(cls, k) ** match)
        return q

    @classmethod
    def find(cls, **kw):
        ''' Find method is looking for exact matches '''
        q = cls.select()
        for k, v in kw.items():
            q.where(getattr(cls, k) == v)
        return [v for v in q]

    @classmethod
    def group_and_avail(cls, condition):
        if not hasattr(cls, 'available'):
            raise TypeError('Invalid object')
        return cls.select(cls,
                          cast(fn.Sum(cls.available), 'int').alias('count'),
                          fn.Count(cls._id).alias('total')).group_by(condition)


class ForeignKeyArrayField(TextField):
    ''' A serialized field that holds foreign fields '''
    def __init__(self, models=None, *args, **kwargs):
        self.models = {cls.__name__: cls for cls in models}
        super(ForeignKeyArrayField, self).__init__(*args, **kwargs)

    def db_value(self, value):
        ''' save to db as a serialized dict: name -> id'''
        if isinstance(value, list):
            print(value)
            # list of models
            data = {model.__class__.__name__: model._id for model in value}
        elif hasattr(value, '_id'):
            # single model
            data = {type(v).__name__: value._id}
        else:
            raise ValueError('Invalid data type')
        return json.dumps(data)

    def python_value(self, value):
        ''' data coming from db.
        This function can raise ValueError if the value in db in not valid
        JSON.  Let it explode so that the user knows something is not right.
        '''
        raw_data = json.loads(value)
        if not isinstance(raw_data, dict):
            raise ValueError('Invalid data received from the database')
        data = []
        for k, v in raw_data.items():
            cls = self.models.get(k, None)
            if not cls:
                raise ValueError('Unsupported model')
            data.append(cls.get(cls._id == v))
        return data


class StaticProperty(AdvancedModel):
    ''' A mixin to use for properties that just have name '''
    name = CharField(max_length=64)

    def to_dict(self):
        return {'_id': self._id, 'name': self.name}

    def __str__(self):
        return self.name


class ProductName(StaticProperty):
    ''' Name of the final product.
    '''
    pass


class ProductGroup(StaticProperty):
    ''' Product type is used to specify the type/group of the final product.
    This can be cake, muffin, set honey delight, etc.
    '''
    pass


class IngredientGroup(StaticProperty):
    ''' This is used to group ingredients. For example raspberry and
    sea buckthorn can be grouped as berries, different honey as just honey.
    '''
    pass


class Supplier(AdvancedModel):
    ''' Supplier of raw ingredients '''
    name = CharField(max_length=256)
    company_name = CharField(max_length=256)
    address = TextField()
    country = CharField(max_length=256)
    email = CharField(max_length=64)
    phone = CharField(max_length=32)
    comment = TextField()
    story = TextField()

    def to_dict(self):
        return {
            '_id': self._id,
            'name': self.name,
            'company_name': self.company_name,
            'address': self.address,
            'country': self.country,
            'email': self.email,
            'phone': self.phone,
            'comment': self.comment,
            'story': self.story}


class IngredientModel(AdvancedModel):
    ''' Base class for components
    '''
    name = CharField(max_length=256)
    group = ForeignKeyField(IngredientGroup)
    price = FloatField(default=0)
    weight = FloatField(default=0)  # in KG
    purchased = DateField(index=True)
    expires = DateField()
    location = CharField(max_length=21)
    location_zoom = IntegerField(default=15)
    supplier = ForeignKeyField(Supplier, index=True)

    def to_dict(self):
        return {
            '_id': self._id,
            'name': self.name,
            'price': self.price,
            'weight': self.weight,
            'supplier': '<{}: {}>'.format(self.supplier.name,
                                          self.supplier._id),
            'purchased': '{}-{}-{}'.format(self.purchased.day,
                                           self.purchased.month,
                                           self.purchased.year),
            'expires': '{}-{}-{}'.format(self.expires.day,
                                         self.expires.month,
                                         self.expires.year)}


class HoneyBatch(IngredientModel):
    ''' Batch of honey
    '''
    def __str__(self):
        return '<Honey: {}, purchased: {}>'.format(self.name, self.purchased)


class BerryBatch(IngredientModel):
    ''' Batch of berries
    '''
    dry_weight = FloatField(default=0)  # in KG
    is_frozen = BooleanField(default=False)

    def __str__(self):
        return '<Berries: {}, purchased: {}>'.format(self.name, self.purchased)

    def to_dict(self):
        d = super(BerryBatch, self).to_dict()
        d['is_frozen'] = self.is_frozen
        return d


class Product(AdvancedModel):
    ''' A representation of a single jar '''
    name = ForeignKeyField(ProductName)  # main ingredient for grouping
    group = ForeignKeyField(ProductGroup)  # set honey, liquid honey
    serial = CharField(max_length=10, index=True)
    created = DateField(index=True)
    expires = DateField()
    weight = FloatField()
    available = BooleanField(default=True)
    components = ForeignKeyArrayField(models=[HoneyBatch, BerryBatch])

    class Meta:
        order_by = ('created',)

    @staticmethod
    def generate_serial():
        return ''.join(random.SystemRandom().choice(
            string.ascii_uppercase + string.digits) for _ in range(12))

    def to_dict(self):
        return {
            '_id': self._id,
            'serial': self.serial,
            'created': '{}-{}-{}'.format(self.created.day,
                                         self.created.month,
                                         self.created.year),
            'expires': '{}-{}-{}'.format(self.expires.day,
                                         self.expires.month,
                                         self.expires.year),
            'type': self.type,
            'weight': self.weight,
            'components': [str(c) for c in self.components]}


class Customer(AdvancedModel):
    ''' Customer model '''
    name = CharField(max_length=256)
    type = CharField(max_length=9)
    address = TextField()
    country = CharField(max_length=256)
    email = CharField(max_length=64)
    phone = CharField(max_length=32)
    comment = TextField()

    def to_dict(self):
        return {
            '_id': self._id,
            'name': self.name,
            'type': self.type,
            'address': self.address,
            'country': self.country,
            'email': self.email,
            'phone': self.phone,
            'comment': self.comment}


class Sale(AdvancedModel):
    ''' A representation of a single purchase '''
    date = DateField(index=True, default=datetime.now)
    product = ForeignKeyField(Product)  # TODO: make it unique
    customer = ForeignKeyField(Customer, related_name='purchases')
    price = FloatField()
    discount = FloatField(default=0)
