import os

from flask import Flask
from flask import request

from api.public import public_blueprint
from api.internal import internal_blueprint
from cms.blueprints.main import cms_blueprint
from cms.blueprints.settings import settings_blueprint

from flask.ext.security import PeeweeUserDatastore, Security
from cms.models import User, Role, UserRoles
from core.database import db


DEBUG = True
SECRET_KEY = ')(Jiq29p8YH#O*&iurhp3iufhwp98fuHP#IUhfP#P(#*HF2p3'
SECURITY_PASSWORD_HASH = 'sha512_crypt'
SECURITY_PASSWORD_SALT = ')(U@JOIKOKLJWDQ(@*Jofi3jf0293ij23flkwefj'
SECURITY_LOGIN_USER_TEMPLATE = 'security/login_user.html'


application = Flask(__name__)
application.config.from_object(__name__)
application.register_blueprint(internal_blueprint, url_prefix='/api')
application.register_blueprint(public_blueprint, url_prefix='/api/public')
application.register_blueprint(cms_blueprint)
application.register_blueprint(settings_blueprint, url_prefix='/settings')

user_datastore = PeeweeUserDatastore(db, User, Role, UserRoles)
security = Security(application, user_datastore)


def create_user():
    for Model in (User, Role, UserRoles):
        Model.drop_table(fail_silently=True)
        Model.create_table(fail_silently=False)
    user_datastore.create_user(email='shipzdik@gmail.com',
                               password='password')


if __name__ == '__main__':
    @application.after_request
    def cors(response):
        response.headers['Access-Control-Allow-Origin'] = \
            request.headers.get('Origin', '*')
        response.headers['Access-Control-Allow-Credentials'] = 'true'
        response.headers['Access-Control-Allow-Methods'] = \
            'POST, GET, PUT, DELETE'
        response.headers['Access-Control-Allow-Headers'] = \
            request.headers.get(
                'Access-Control-Request-Headers', 'Authorization')
        return response

    application.run()
