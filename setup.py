from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import sys

class Tox(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True
    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import tox
        errcode = tox.cmdline(self.test_args)
        sys.exit(errcode)

setup(name='Honey Web',
      version='0.1',
      description='A simple ERP system',
      packaged=find_packages(),
      install_requires=[
          'flask',
          'wtforms',
          'peewee',
          'tox',
          'pytest',
          'pytest-cov'],
      tests_require=['tox'],
      cmdclass = {'test': Tox},
)
